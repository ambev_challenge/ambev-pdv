FROM openjdk:8

LABEL SERVICE_NAME=ambev-pdv
ENV APP_NAME ambev-pdv.jar
ENV USER_NAME service
ENV APP_HOME /opt/ambev/$USER_NAME

ENV LANG pt_BR.UTF-8

RUN mkdir -p $APP_HOME

ADD target/*.jar ${APP_HOME}/${APP_NAME}

RUN sh -c 'touch $APP_HOME/$APP_NAME'

WORKDIR $APP_HOME

ENTRYPOINT [ "sh", "-c", "java -jar $APP_NAME" ]
ENV TZ 'America/Sao_Paulo'