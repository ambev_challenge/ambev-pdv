package com.ambev.pdv.databuilders.domain;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.ambev.pdv.core.entities.Address;

public class AddressTemplate {
    public static void loadTemplates() {
        anyAddress();
    }

    public static final String ANY_ADDRESS = "ANY_ADDRESS";
    private static void anyAddress() {
        Fixture.of(Address.class).addTemplate(ANY_ADDRESS, new Rule() {
            {
                add("type", "Point");
                add("coordinates", new Double[]{ -43.124176, -22.902254});
            }
        });
    }
}
