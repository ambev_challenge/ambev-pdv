package com.ambev.pdv.databuilders.domain;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.ambev.pdv.core.entities.CoverageArea;

public class CoverageAreaTemplate {
    public static void loadTemplates() {
        anyCoverageArea();
    }

    public static final String ANY_COVERAGE_AREA = "ANY_COVERAGE_AREA";
    private static void anyCoverageArea() {
        Fixture.of(CoverageArea.class).addTemplate(ANY_COVERAGE_AREA, new Rule() {
            {
                add("type", "MultiPolygon");
                add("coordinates", new Double[][][][] {{{ { -43.124176, -22.902254}, { -43.124176, -22.902254}, { -43.124176, -22.902254}, { -43.124176, -22.902254} }}});
            }
        });
    }
}
