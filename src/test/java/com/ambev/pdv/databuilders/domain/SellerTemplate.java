package com.ambev.pdv.databuilders.domain;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.ambev.pdv.core.entities.Address;
import com.ambev.pdv.core.entities.CoverageArea;
import com.ambev.pdv.core.entities.Seller;

public class SellerTemplate {
    public static void loadTemplates() {
        anySeller();
    }

    public static final String ANY_SELLER = "ANY_SELLER";
    private static void anySeller() {
        Fixture.of(Seller.class).addTemplate(ANY_SELLER, new Rule() {
            {
                add("id", "1");
                add("tradingName", "Bar Teste");
                add("ownerName", "Teste Dono");
                add("document", "06.269.410/0001-19");
                add("coverageArea", one(CoverageArea.class, CoverageAreaTemplate.ANY_COVERAGE_AREA));
                add("address", one(Address.class, AddressTemplate.ANY_ADDRESS));
            }
        });
    }
}
