package com.ambev.pdv.databuilders;

import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.ambev.pdv.databuilders.json.SellerRequestTemplate;

public class JsonTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        SellerRequestTemplate.loadTemplates();
    }
}
