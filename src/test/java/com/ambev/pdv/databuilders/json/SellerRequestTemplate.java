package com.ambev.pdv.databuilders.json;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import com.ambev.pdv.http.json.SellerRequest;

public class SellerRequestTemplate {

    public static void loadTemplates() {
        anySellerRequest();
    }

    public static final String ANY_SELLER_REQUEST = "ANY_SELLER_REQUEST";
    private static void anySellerRequest() {
        Fixture.of(SellerRequest.class).addTemplate(ANY_SELLER_REQUEST, new Rule() {
            {
                add("id", "1");
                add("tradingName", "Bar Teste");
                add("ownerName", "Teste Dono");
                add("document", "06.269.410/0001-19");
                add("coverageArea", new Double[][][][] {{{ { -43.124176, -22.902254}, { -43.124176, -22.902254}, { -43.124176, -22.902254}, { -43.124176, -22.902254} }}});
                add("address", new Double[]{ -43.124176, -22.902254});
            }
        });
    }
}
