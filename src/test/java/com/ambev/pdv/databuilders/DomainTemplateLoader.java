package com.ambev.pdv.databuilders;

import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.ambev.pdv.databuilders.domain.AddressTemplate;
import com.ambev.pdv.databuilders.domain.CoverageAreaTemplate;
import com.ambev.pdv.databuilders.domain.SellerTemplate;

public class DomainTemplateLoader implements TemplateLoader {

    @Override
    public void load() {
        SellerTemplate.loadTemplates();
        AddressTemplate.loadTemplates();
        CoverageAreaTemplate.loadTemplates();
    }
}
