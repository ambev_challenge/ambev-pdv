package com.ambev.pdv.core.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.databuilders.DomainTemplateLoader;
import com.ambev.pdv.databuilders.JsonTemplateLoader;
import com.ambev.pdv.databuilders.domain.SellerTemplate;
import com.ambev.pdv.databuilders.json.SellerRequestTemplate;
import com.ambev.pdv.dataproviders.PointOfSaleDataBaseDataProvider;
import com.ambev.pdv.http.json.SellerRequest;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.util.ClassUtils;
import reactor.core.publisher.Mono;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SaveSellerTest {
    @InjectMocks
    private SaveSeller usecase;

    @Mock
    private PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(JsonTemplateLoader.class));
    }

    @Test
    public void shoudSaveSellerWithSuccess() {
        final SellerRequest request = Fixture.from(SellerRequest.class).gimme(SellerRequestTemplate.ANY_SELLER_REQUEST);
        final Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        when(pointOfSaleDataBaseDataProvider.save(any(Seller.class))).thenReturn(Mono.just(seller));

        Mono<Seller> saved = usecase.save(request);

        assertNotNull(saved);
        assertEquals(seller, saved.block());
        verify(pointOfSaleDataBaseDataProvider, VerificationModeFactory.times(1)).save(any(Seller.class));
    }

}