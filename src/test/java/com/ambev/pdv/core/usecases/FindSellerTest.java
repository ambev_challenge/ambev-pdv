package com.ambev.pdv.core.usecases;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.databuilders.DomainTemplateLoader;
import com.ambev.pdv.databuilders.domain.SellerTemplate;
import com.ambev.pdv.dataproviders.PointOfSaleDataBaseDataProvider;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.util.ClassUtils;
import reactor.core.publisher.Flux;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class FindSellerTest {
    @InjectMocks
    private FindSeller usecase;

    @Mock
    private PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
    }

    @Test
    public void shoudFoundNearSellerWithSuccess() {
        final Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        when(pointOfSaleDataBaseDataProvider.findNearByLocation(any(Double.class), any(Double.class))).thenReturn(Flux.just(seller));

        Flux<Seller> found = usecase.findNear(-43.124176, -22.902254);

        assertNotNull(found);
        assertEquals(seller, found.blockFirst());
        verify(pointOfSaleDataBaseDataProvider, VerificationModeFactory.times(1)).findNearByLocation(-43.124176, -22.902254);
    }
}