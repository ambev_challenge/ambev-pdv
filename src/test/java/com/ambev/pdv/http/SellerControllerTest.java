package com.ambev.pdv.http;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.core.usecases.FindSeller;
import com.ambev.pdv.core.usecases.SaveSeller;
import com.ambev.pdv.databuilders.DomainTemplateLoader;
import com.ambev.pdv.databuilders.JsonTemplateLoader;
import com.ambev.pdv.databuilders.domain.SellerTemplate;
import com.ambev.pdv.databuilders.json.SellerRequestTemplate;
import com.ambev.pdv.http.json.SellerRequest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.ClassUtils;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = SellerController.class)
public class SellerControllerTest {
    private static final String URL_TEMPLATE = "/api/seller";
    @Autowired
    private WebTestClient client;

    @MockBean
    private SaveSeller saveSeller;

    @MockBean
    private FindSeller findSeller;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(JsonTemplateLoader.class));
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
    }

//    @Test
//    public void constructorTest(){
//        SellerController c = new SellerController(saveSeller, findSeller);
//        assertThat(, is(c.));
//    }

    @Test
    public void shouldCreateSellerWithSuccess() {
        final SellerRequest request = Fixture.from(SellerRequest.class).gimme(SellerRequestTemplate.ANY_SELLER_REQUEST);
        final Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        given(saveSeller.save(BDDMockito.any(SellerRequest.class))).willReturn(Mono.just(seller));

        client.post().uri(URL_TEMPLATE).body(BodyInserters.fromObject(request))
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isEqualTo(seller.getId())
                .jsonPath("$.tradingName").isEqualTo(seller.getTradingName())
                .jsonPath("$.ownerName").isEqualTo(seller.getOwnerName())
                .jsonPath("$.document").isEqualTo(seller.getDocument());
    }

    @Test
    public void shouldFindNearSellerWithSuccess() {
        final SellerRequest request = Fixture.from(SellerRequest.class).gimme(SellerRequestTemplate.ANY_SELLER_REQUEST);
        final Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        given(findSeller.findNear(BDDMockito.any(Double.class), BDDMockito.any(Double.class))).willReturn(Flux.just(seller));

       client.get().uri(uriBuilder -> uriBuilder.path(URL_TEMPLATE)
                .queryParam("longitude", request.getAddress()[0])
               .queryParam("latitude", request.getAddress()[1])
                .build())
                .exchange()
                .expectStatus().isOk()
                .expectBody()
               .jsonPath("$[0].id").isEqualTo(seller.getId())
                .jsonPath("$[0].tradingName").isEqualTo(seller.getTradingName())
                .jsonPath("$[0].ownerName").isEqualTo(seller.getOwnerName())
                .jsonPath("$[0].document").isEqualTo(seller.getDocument());
    }

    @Test
    public void shouldFindSellerByIdWithSuccess() {
        final SellerRequest request = Fixture.from(SellerRequest.class).gimme(SellerRequestTemplate.ANY_SELLER_REQUEST);
        final Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        given(findSeller.findSellerById(BDDMockito.any(String.class))).willReturn(Mono.just(seller));

        client.get().uri(URL_TEMPLATE.concat("/24302190000160"))
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo(seller.getId())
                .jsonPath("$.tradingName").isEqualTo(seller.getTradingName())
                .jsonPath("$.ownerName").isEqualTo(seller.getOwnerName())
                .jsonPath("$.document").isEqualTo(seller.getDocument());
    }

}