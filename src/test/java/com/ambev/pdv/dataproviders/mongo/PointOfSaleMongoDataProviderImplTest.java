package com.ambev.pdv.dataproviders.mongo;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.databuilders.DomainTemplateLoader;
import com.ambev.pdv.databuilders.domain.SellerTemplate;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.util.ClassUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PointOfSaleMongoDataProviderImplTest {
    @Mock
    private PointOfSaleRepository repository;

    @InjectMocks
    private PointOfSaleMongoDataProviderImpl dataProvider;

    @BeforeClass
    public static void setUp() {
        FixtureFactoryLoader.loadTemplates(ClassUtils.getPackageName(DomainTemplateLoader.class));
    }

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldSaveWithSuccess() {
        Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        when(repository.save(any(Seller.class))).thenReturn(Mono.just(seller));

        Mono<Seller> saved = dataProvider.save(seller);

        assertSeller(seller, saved.block());

        verify(repository, VerificationModeFactory.times(1)).save(seller);
    }

    private void assertSeller(Seller seller, Seller saved) {
        assertNotNull(saved);
        assertEquals(seller.getDocument(), saved.getDocument());
        assertEquals(seller.getId(), saved.getId());
    }

    @Test
    public void shouldFoundNearSellerWithSuccess() {
        Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        when(repository.findNear(any(Double.class), any(Double.class), any(Long.class))).thenReturn(Flux.just(seller));

        Flux<Seller> found = dataProvider.findNearByLocation(-43.124176, -22.902254);

        assertSeller(seller, found.blockFirst());

        verify(repository, VerificationModeFactory.times(1)).findNear(-43.124176, -22.902254, 659999L);
    }

    @Test
    public void shouldNotFoundNearSeller() {
        Seller seller = Fixture.from(Seller.class).gimme(SellerTemplate.ANY_SELLER);

        when(repository.findNear(any(Double.class), any(Double.class), any(Long.class))).thenReturn(Flux.empty());

        Flux<Seller> found = dataProvider.findNearByLocation(-43.124176, -22.902254);

        assertNull(found.blockFirst());

        verify(repository, VerificationModeFactory.times(1)).findNear(-43.124176, -22.902254, 659999L);
    }
}