package com.ambev.pdv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmbevPdvApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmbevPdvApplication.class, args);
    }

}
