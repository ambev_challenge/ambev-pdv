package com.ambev.pdv.core.usecases;

import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.core.mapper.SellerEntityMapper.SellerMapper;
import com.ambev.pdv.dataproviders.PointOfSaleDataBaseDataProvider;
import com.ambev.pdv.http.json.SellerRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class SaveSeller {

    private final PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider;

    public SaveSeller(PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider) {
        this.pointOfSaleDataBaseDataProvider = pointOfSaleDataBaseDataProvider;
    }

    public Mono<Seller> save(final SellerRequest request) {
        final Seller seller = SellerMapper.MAPPER.sellerRequestToSellerSameFields(request);
        return pointOfSaleDataBaseDataProvider.save(seller);
    }
}
