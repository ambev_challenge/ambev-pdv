package com.ambev.pdv.core.usecases;

import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.dataproviders.PointOfSaleDataBaseDataProvider;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class FindSeller {
    private final PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider;

    public FindSeller(PointOfSaleDataBaseDataProvider pointOfSaleDataBaseDataProvider) {
        this.pointOfSaleDataBaseDataProvider = pointOfSaleDataBaseDataProvider;
    }

    public Flux<Seller> findNear(final double first, final double second) {
        return pointOfSaleDataBaseDataProvider.findNearByLocation(first, second);
    }

    public Mono<Seller> findSellerById(final String id) {
        return pointOfSaleDataBaseDataProvider.findSellerById(id);
    }
}
