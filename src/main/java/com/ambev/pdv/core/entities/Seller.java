package com.ambev.pdv.core.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pdvs")
@Getter
@Setter
@EqualsAndHashCode(exclude = { "tradingName", "ownerName"})
public class Seller {
    @Id
    private String id;

    private String tradingName;

    private String ownerName;

    @Indexed(unique = true)
    private String document;

    private CoverageArea coverageArea;

    private Address address;
}
