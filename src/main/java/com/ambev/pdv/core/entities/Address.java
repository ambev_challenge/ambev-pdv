package com.ambev.pdv.core.entities;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(exclude = "type")
public class Address {
    private String type;
    private Double[] coordinates;
}
