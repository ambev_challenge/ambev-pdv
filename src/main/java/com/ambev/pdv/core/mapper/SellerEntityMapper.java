package com.ambev.pdv.core.mapper;

import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.http.json.SellerRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SellerEntityMapper {

    @Mappings({
            @Mapping(target="coverageArea.coordinates", source="request.coverageArea"),
            @Mapping(target="address.coordinates", source="request.address"),
            @Mapping(target="coverageArea.type", constant = "MultiPolygon"),
            @Mapping(target="address.type", constant = "Point")

    })
    Seller sellerRequestToSellerSameFields(SellerRequest request);

    enum SellerMapper {
        ;
        public static final SellerEntityMapper MAPPER = Mappers.getMapper(SellerEntityMapper.class);
    }

}
