package com.ambev.pdv.http.json;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class SellerRequest implements Serializable {
    private static final long serialVersionUID = 8249586304055168456L;

    @NotNull
    @ApiModelProperty(required = true)
    private String id;

    @NotNull
    @ApiModelProperty(required = true)
    private String tradingName;

    @NotNull
    @ApiModelProperty(required = true)
    private String ownerName;

    @NotNull
    @ApiModelProperty(required = true)
    private String document;

    @NotNull
    @ApiModelProperty(required = true)
    private Double[][][][] coverageArea;

    @NotNull
    @ApiModelProperty(required = true)
    private Double[] address;
}
