package com.ambev.pdv.http;

import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.core.usecases.FindSeller;
import com.ambev.pdv.core.usecases.SaveSeller;
import com.ambev.pdv.http.json.SellerRequest;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/seller")
@Api(tags = "Controller to manage sellers", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class SellerController {

    private SaveSeller saveSeller;

    private FindSeller findSeller;

    public SellerController(FindSeller findSeller) {
        this.findSeller = findSeller;
    }

    @Autowired
    private SellerController(SaveSeller saveSeller, FindSeller findSeller) {
        this.saveSeller = saveSeller;
        this.findSeller = findSeller;
    }

    @ApiOperation(value = "Resource to create seller", response = Seller.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Seller> create(@ApiParam(name = "sellerRequest", value = "Seller object that will be created", required = true)
                               @RequestBody @Valid final SellerRequest request) {
        log.debug("Try to create seller: request {}", request);

        return saveSeller.save(request)
                .doOnNext(seller -> log.debug("Create new seller - {}", seller.getDocument()));
    }

    @ApiOperation(value = "Resource to find a near seller by long and lat", response = Seller.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ok"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Flux<Seller> findNear(@ApiParam(name = "longitude", required = true) @RequestParam final double longitude,
                                 @ApiParam(name = "latitude", required = true) @RequestParam final double latitude) {
        log.debug("Find seller most near to longitude {} and latitude {}", longitude, latitude);

        return findSeller.findNear(longitude, latitude)
                .doOnNext(seller -> log.debug("Seller found - {}", seller.getDocument()));
    }

    @ApiOperation(value = "Resource to create seller", response = Seller.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 422, message = "Unprocessable Entity"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Mono<Seller> findById(@PathVariable("id") final String id) {
        log.debug("Find seller by id {}", id);

        return findSeller.findSellerById(id);
    }
}
