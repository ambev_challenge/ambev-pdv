package com.ambev.pdv.dataproviders.exception;


public class DataNotFoundDataProviderException extends RuntimeException {
    private static final String MESSAGE =
            "It was not possible to find a seller.";

    public DataNotFoundDataProviderException() {
        super(MESSAGE);
    }
}
