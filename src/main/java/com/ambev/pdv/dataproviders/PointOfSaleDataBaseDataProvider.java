package com.ambev.pdv.dataproviders;

import com.ambev.pdv.core.entities.Seller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PointOfSaleDataBaseDataProvider {

    Mono<Seller> save(final Seller seller);

    Flux<Seller> findNearByLocation(double first, double second);

    Mono<Seller> findSellerById(String document);
}
