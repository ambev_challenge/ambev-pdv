package com.ambev.pdv.dataproviders.mongo;

import com.ambev.pdv.core.entities.Seller;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface PointOfSaleRepository extends ReactiveMongoRepository<Seller, String> {
  @Query(
      value =
          "{address: {\n"
              + "     $near: {\n"
              + "       $geometry: {\n"
              + "          type: \"Point\" ,\n"
              + "          coordinates: [ ?0, \n"
              + "            ?1 ]\n"
              + "       },\n"
              + "       $maxDistance: ?2,\n"
              + "       $minDistance: 0\n"
              + "     }\n"
              + "   }\n"
              + "}")
  Flux<Seller> findNear(final double lat, final double log, final long accuracy);
}
