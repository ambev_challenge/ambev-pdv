package com.ambev.pdv.dataproviders.mongo;

import com.ambev.pdv.core.entities.Seller;
import com.ambev.pdv.dataproviders.PointOfSaleDataBaseDataProvider;
import com.ambev.pdv.dataproviders.exception.DataNotFoundDataProviderException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class PointOfSaleMongoDataProviderImpl implements PointOfSaleDataBaseDataProvider {

    private final PointOfSaleRepository repository;

    @Autowired
    private PointOfSaleMongoDataProviderImpl(PointOfSaleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<Seller> save(final Seller seller) {
        return repository.save(seller);
    }

    @Override
    public Flux<Seller> findNearByLocation(double first, double second) {
        Flux<Seller> take = repository.findNear(first, second, 659999L).take(1);
        if(take == null) {
            throw new DataNotFoundDataProviderException();
        }

        return take;
    }

    @Override
    public Mono<Seller> findSellerById(final String id) {
        return repository.findById(id);
    }

}
