# Seller management API


## Requisitos de sistema

Para gerar e rodar a aplicaÃ§Ã£o vocÃª vai precisar ter instalado:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (local)
- [Maven 3](https://maven.apache.org) (local)
- [Docker](https://docs.docker.com/)

## Rodando a aplicaÃ§Ã£o local

**IMPORTANTE:**
Antes de executar a aplicaÃ§Ã£o, suba o container mongo no repositório ambev-devops (https://gitlab.com/ambev_challenge/ambev-devops)
``` docker-compose up --build -d
```
Existem vÃ¡rias maneiras de executar um aplicativo Spring Boot em sua mÃ¡quina local.

Alternativamente, vocÃª pode usar o [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) com o comando:

```shell
mvn spring-boot:run
```

Outra forma Ã© gerar a imagem docker e rodar o container local. Para isso Ã© necessÃ¡rio instalar o docker e seguir os comandos:

```shell
docker build -t ambev/app .
```

Ao receber a mensagem de sucesso execute, na pasta de ambev-devops/docker-images

```shell
docker-compose up app -d
```
A aplicaÃ§Ã£o ficara expota na porta 8080.

## Rodando a aplicaÃ§Ã£o nos container

Toda os recursos da aplicaÃ§Ã£o pode ser executado na pasta de devops, com os seguintes comandos no docker-compose.yml;

```
docker-compose build && docker-compose up
```

Para desligar
```
docker-compose down
``` 

Detalhes da API no endereÃ§o do swagger

[swagger-ui](http://localhost:8080/swagger-ui.html)

### Rodando os testes da aplicaÃ§Ã£o
Execute o comando:
```
mvn clean test
```

Testes unitÃ¡rios na camada de negÃ³cio e conecxÃ£o com banco de dados.
84% de cobertura desconsiderando classes de domain, json e exception.

RElatorio na pasta do Jacoco, após build com test

### SoluÃ§Ã£o da aplicaÃ§Ã£o
